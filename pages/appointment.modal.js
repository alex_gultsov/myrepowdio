/**
 * Created by agultsov on 09/01/18.
 */

let Page = require('./page');
let HomePage = require('./home.page');
let helper = require('../utils/helper');

class AppointmentModal extends (Page) {
    constructor () {
        super();



        // selectors
        this.frameToUseSelector = 'iframe[id="bookAnAppointment"]';
        this.phoneServiceBtnSelector = "//label[@for='phoneService']";

        this.dateDropDownSelector = "#choosenDatePlaceholder";
        this.timeDropDownSelector = "#choosenTime";

        this.nameInputSelector = "#name";
        this.emailInputSelector = "#email";
        this.phoneInputSelector = "#phone";
        this.commentsTextAreaSelector = "#extraInfo";

        this.checkBoxSubscribeSelector = "#autoSubscribe";

        this.submitBtnSelector = "//button[@type='submit']";
        this.closeBtnSelector = "//div[@id='appointmentModal']/button";
    }


    get phoneBtn() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.phoneServiceBtnSelector);
        return $(this.phoneServiceBtnSelector);
    }

    get dateDropDown() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.dateDropDownSelector);
        return $(this.dateDropDownSelector);
    }

    get timeDropDown() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.timeDropDownSelector);
        return $(this.timeDropDownSelector);
    }


    get nameInput() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.nameInputSelector);
        return $(this.nameInputSelector);
    }

    get emailInput() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.emailInputSelector);
        return $(this.emailInputSelector);
    }

    get phoneInput() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.phoneInputSelector);
        return $(this.phoneInputSelector);
    }

    get commentsTextArea() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.commentsTextAreaSelector);
        return $(this.commentsTextAreaSelector);
    }

    get checkBoxSubscribe() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.checkBoxSubscribeSelector);
        return $(this.checkBoxSubscribeSelector);
    }

    get submitBtn() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.submitBtnSelector);
        return $(this.submitBtnSelector);
    }

    get closeBtn() {
        this.switchToFrameParent();
        helper.waitForElementPresence(this.closeBtnSelector);
        return $(this.closeBtnSelector);
    }

    setTime(value) {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.timeDropDownSelector);
        $(this.timeDropDownSelector).selectByValue(value);
    }

    markCheckBoxSubscribe() {
        let isSelected = this.checkBoxSubscribe.isSelected();

       if(isSelected === false) {
           this.checkBoxSubscribe.click();
       }
    }

    clickOnSubmitBtn() {
        this.submitBtn.waitForEnabled();
        this.submitBtn.click();
        // return new HomePage();
    }

    clickOnCloseBtn() {
        this.closeBtn.click();
        // return new HomePage();
    }

}

module.exports = AppointmentModal;