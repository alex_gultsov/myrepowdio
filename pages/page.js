/**
 * Created by agultsov on 08/31/18.
 */
let helper = require('../utils/helper');
class Page  {
    open (path) {
        browser.url(path)
    }

    switchToFrame(selector) {
        // browser.waitForExist(selector);
        helper.waitForElementPresence(selector);
        let my_frame = $(selector).value;
        browser.frame(my_frame);
    }

    switchToFrameParent() {
        browser.frameParent();
    }
}

module.exports = Page;
