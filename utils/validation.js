/**
 * Created by agultsov on 09/02/18.
 */

let helper = require('./helper');

// getting the title
//span[@class='global-services__validation__title']
// or $('.global-services__validation__title')
const getValidationTitleElement = function () {
    let selectorToFind = '.global-services__validation__title' ;
    // let selectorToFind = "//span[@class='global-services__validation__title']" ;
    // browser.waitForExist(selectorToFind, 1500);
    helper.waitForElementPresence(selectorToFind);
    return $(selectorToFind);
};


// getting the text
//span[@class='global-services__validation__text']
// or $('.global-services__validation__text')
const getValidationTextElement = function () {
    let selectorToFind = '.global-services__validation__text' ;
    // let selectorToFind = "//span[@class='services__validation__text']" ;
    helper.waitForElementPresence(selectorToFind);
    return $(selectorToFind);
};

exports.getValidationTitleElement = getValidationTitleElement;
exports.getValidationTextElement = getValidationTextElement;