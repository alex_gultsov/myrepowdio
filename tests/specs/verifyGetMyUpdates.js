/**
 * Created by agultsov on 09/01/18.
 */

let HomePage = require('../../pages/home.page');

let validation = require('../../utils/validation');
let helper = require('../../utils/helper');

let assert = require('assert');

describe('Verify Get My Updates URL.', () =>  {
    let emailToSpecify = "MrTest@gmail.com";
    let nameToSpecify = "Mr Test";

    it('As a customer, I can subscribe on Get My Updates.',  () => {
        let homePage = new HomePage();

        homePage.open();

        let getUpdatesModal = homePage.clickOnGetUpdates();

        getUpdatesModal.emailInput.setValue(emailToSpecify);


        getUpdatesModal.nameInput.setValue(nameToSpecify);

        getUpdatesModal.clickOnSubmitBtn();

        let actualTitle = validation.getValidationTitleElement().getText();

        let titleToVerify = "Thank you!";
        assert.equal(actualTitle, titleToVerify, "Verify:" + titleToVerify);


        let actualText = validation.getValidationTextElement().getText();
        let textToVerify = "You are now signed up";
        assert.equal(actualText, textToVerify, "Verify:" + textToVerify);

    });


    let assertMsgSubmitDisabled = "Verify submit button disabled.";

    // Negative case for empty email field
    it('It is unable to subscribe on Get My Updates in case of empty email field.',  () => {
        let homePage = new HomePage();
        homePage.open();

        let getUpdatesModal = homePage.clickOnGetUpdates();

        let emptyEmail = "";

        getUpdatesModal.emailInput.setValue(emptyEmail);

        getUpdatesModal.nameInput.setValue(nameToSpecify);

        assert.equal(getUpdatesModal.submitBtn.isEnabled(), false,  assertMsgSubmitDisabled + " Case: 'empty email field'.");
    });

    // Negative case for email field without '@'
    let wrongEmailFrmt = "MrTest_gmail.com";

    it('It is unable to subscribe on Get My Updates in case of specifying email field in wrong format.',  () => {
        let homePage = new HomePage();
        homePage.open();

        let getUpdatesModal = homePage.clickOnGetUpdates();

        getUpdatesModal.emailInput.setValue(wrongEmailFrmt);

        getUpdatesModal.nameInput.setValue(nameToSpecify);

        assert.equal(getUpdatesModal.submitBtn.isEnabled(), false, assertMsgSubmitDisabled + " Case: 'email field in wrong format'.");
    });

    // Negative case for email field with email which is already used
    let alreadyUsedEmail = emailToSpecify;

    it('It is unable to subscribe on Get My Updates in case of specifying email field which was already used.',  () => {
        let homePage = new HomePage();
        homePage.open();

        let getUpdatesModal = homePage.clickOnGetUpdates();

        getUpdatesModal.emailInput.setValue(alreadyUsedEmail);

        getUpdatesModal.nameInput.setValue(nameToSpecify);

        assert.equal(getUpdatesModal.submitBtn.isEnabled(), false, assertMsgSubmitDisabled + " Case: 'email field which was already used'.");
    });
});