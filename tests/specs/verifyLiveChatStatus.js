/**
 * Created by agultsov on 09/01/18.
 */

let assert = require('assert');

let HomePage = require('../../pages/home.page');

describe('Verify Live Chat statu.', () =>  {
    it('As a customer, I can see the “LIVE CHAT” status as either “Available” or “Unavailable”.', () => {
        let homePage = new HomePage();

        homePage.open();

        homePage.chatStatus.waitForExist();

        // lets check the status is visible
        assert.equal(true, homePage.chat.isVisible(), "Verify the Live Chat status is visible, isVisible=" + homePage.chatStatus.isVisible());

        // such color definitions can be moved to file constants or smth like that
        let CHAT_STATUS = {
            UNAVAILABLE : {name: "Unavailable", COLOR: {hex: "#ee0c0c", value: "rgba(238,12,12,1)"}}, // red
            AVAILABLE : {name: "Available", COLOR: {hex: "#46a629", value: "rgba(70,166,41,1)"}} // green
        };

        let actualChatColorValue = homePage.getActiveChatStatus().getCssProperty('color').value;

        // Let check the status text and color
        switch (homePage.chatStatus.getText()) {
            case CHAT_STATUS.UNAVAILABLE.name: {
                assert.equal(actualChatColorValue, CHAT_STATUS.UNAVAILABLE.COLOR.value, "Verify:" + CHAT_STATUS.UNAVAILABLE.name);
                break;
            }
            case CHAT_STATUS.AVAILABLE.name: {
                assert.equal(actualChatColorValue, CHAT_STATUS.AVAILABLE.COLOR.value, "Verify:" + CHAT_STATUS.AVAILABLE.name);
                break;
            }
            default: {
                // in case we are here test should be failed
                let errMsg = "Error is occurred during verifying Live Chat status spelling.";
                assert.fail(errMsg);
            }
        }

    });
});
