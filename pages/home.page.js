/**
 * Created by agultsov on 08/31/18.
 */


let Page = require('./page');

let LiveChat = require('./liveChat.modal');
let AppointmentModal = require('./appointment.modal');
let GetUpdatesModal = require('./getUpdates.modal');

let helper = require('../utils/helper');

class HomePage  extends (Page) {
    constructor() {
        super();

        // selectors
        this.liveChatSelector = '#chat';
        this.chatStatusSelector = '#AtChatStatus';
        // this.chatStatusSelector = "//div[@id='AtChatStatus']/span[contains(@class, 'is-active')]"; //div[@id='AtChatStatus']/span[contains(@class, 'is-active')]

        this.appointmentSelector = '#AtAppointmentLink';


        this.getUpdatesSelector = "//footer//li/a[contains(@data-modal-open,'inscriptionModal')]";

    }

    /**
     * define or overwrite page methods
     */
    open() {
        let urlToNavigate = 'https://elguntors-stg.salesfloor.net/reggie';
        super.open(urlToNavigate);
    }

    getTitle() {
        return browser.getTitle();
    }

    get chat() {return $(this.liveChatSelector)}

    get chatStatus() {return $(this.chatStatusSelector)}

    get appointment() {return $(this.appointmentSelector)}

    get getUpdates() {
        helper.waitForElementPresence(this.getUpdatesSelector);
        helper.waitForElementVisible(this.getUpdatesSelector);
        return $(this.getUpdatesSelector)}

    clickOnLiveChat() {
        this.chat.click();
        return new LiveChat();
    }

    clickOnAppointment() {
        this.appointment.click();
        return new AppointmentModal();
    }


    clickOnGetUpdates() {
        this.getUpdates.waitForExist();
        this.getUpdates.waitForVisible();
        this.getUpdates.click();
        return new GetUpdatesModal();
    }


    getActiveChatStatus() {
        let activeChatXpath = "./span[contains(@class, 'is-active')]";
        return this.chatStatus.$(activeChatXpath);
    }
 }

module.exports = HomePage;
