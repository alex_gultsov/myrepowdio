/**
 * Created by agultsov on 08/31/18.
 */

// let chai = require('chai');
// let assert = chai.assert;
// let expect = chai.expect;
// let should = chai.should;
let assert = require('assert');

let HomePage = require('../../pages/home.page');

describe('Verify page is opened and data presents.', () =>  {
    it('As a customer, I can access Storefront', () => {
        let homePage = new HomePage();
        homePage.open();

        // lets check the page title
        let titleActual = homePage.getTitle();
        let titleToVerify = "Shop with Sally Sellers - El Guntors";
        assert.equal(titleActual, titleToVerify);


        // check the Live Chat link is enabled
        assert.equal(true, homePage.chat.isEnabled(), "Verify the Live Chat link is enabled");


        // check the Appointment link is enabled
        assert.equal(true, homePage.appointment.isEnabled(), "Verify the Appointment link is enabled");

    });
});


