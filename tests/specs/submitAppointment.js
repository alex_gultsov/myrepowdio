/**
 * Created by agultsov on 09/01/18.
 */
let HomePage = require('../../pages/home.page');

let validation = require('../../utils/validation');

describe('Verify Appointment.', () =>  {
    it('As a customer, I can submit Appointment',  () => {
        let homePage = new HomePage();

        homePage.open();

        let appointmentModal = homePage.clickOnAppointment();

        appointmentModal.phoneBtn.click();


        appointmentModal.dateDropDown.click();

        // --> actually here also should be method to choose the date
        // decided to simplify a little...
        let elToChoose = $("//table[@class='ui-datepicker-calendar']//a[.='9']");
        elToChoose.click();
        // <--

        let timeToChoose = '11:00 AM';
        appointmentModal.timeDropDown.selectByVisibleText(timeToChoose);


        let nameToSpecify = "Mr Test";
        appointmentModal.nameInput.setValue(nameToSpecify);


        let emailToSpecify = "MrTest@gmail.com";
        appointmentModal.emailInput.setValue(emailToSpecify);


        let phoneToSpecify = "1-800-437-7950";
        appointmentModal.phoneInput.setValue(phoneToSpecify);

        let textAreaToSpecify = "Let's leave comment here!";
        appointmentModal.commentsTextArea.setValue(textAreaToSpecify);

        appointmentModal.markCheckBoxSubscribe();

        appointmentModal.clickOnSubmitBtn();

        // Check for “THANK YOU FOR YOUR REQUEST” to pass the test case.
        // actually for now it prompts to specify phone number again and again, and we don't know how the dialog should be looks like.
        // So the test case will be failed.

        let actualTitle = validation.getValidationTitleElement().getText();

        let titleToVeify = "THANK YOU FOR YOUR REQUEST";
        assert.equal(actualTitle, titleToVeify, "Verify:" + titleToVeify);


        appointmentModal.clickOnCloseBtn();
    });
});
