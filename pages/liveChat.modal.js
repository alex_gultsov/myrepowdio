/**
 * Created by agultsov on 08/31/18.
 */
let Page = require('./page');
class LiveChat extends (Page) {
    constructor() {
        super();

        this.emailInputSelector = '#email';

    }

    get emailInput() {return $(this.emailInputSelector)}

}

module.exports = LiveChat;