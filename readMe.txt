# `myrepowdio` — Webdriver IO project for end to end automated testing for the application.

## Directory Layout

```
pages           --> page objects
tests/specs/    --> end-to-end tests
utils           --> test's 'framework' useful files
wdio.conf.js    --> Webdriver IO configuration file
```

# There are 4 automated tests in directory "tests/specs/":

##
'accessToStorefront.js'

Verifies the home page title, links live chat and appointment are enabled.

##
'verifyLiveChatStatus.js'

Verifies the “LIVE CHAT” status is visible. In case of it 'Unavailable' validate the red color,
in case of it 'Available' validate the green color.

##
'submitAppointment.js'

Verifies the possibility to submit Appointment. Actually, test won't pass due to issue in application.

##
'verifyGetMyUpdates.js'

Verifies the possibility to subscribe on Updates Appointment.

Also, the test contains three negative cases:
- specify empty value in email field
- specify invalid value in email field
- specify already used value in email field