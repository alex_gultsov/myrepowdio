/**
 * Created by agultsov on 09/02/18.
 */
let Page = require('./page');
// let Modal = require('./modal');

let HomePage = require('./home.page');
let helper = require('../utils/helper');

class GetUpdatesModal extends Page/*, Modal*/ {
    constructor () {
        super();


        // selectors
        this.frameToUseSelector = 'iframe[id="inscription"]';

        this.emailInputSelector = "#newsletterInscEmail";
        this.nameInputSelector = "#inscName";


        this.closeBtnSelector = "//div[@id='inscriptionModal']/button";
        this.submitBtnSelector = "//button[@type='submit']";

    }


    get emailInput() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.emailInputSelector);
        return $(this.emailInputSelector);
    }

    get nameInput() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.nameInputSelector);
        return $(this.nameInputSelector);
    }

    get closeBtn() {
        this.switchToFrameParent();
        helper.waitForElementPresence(this.closeBtnSelector);
        return $(this.closeBtnSelector);
    }

    get submitBtn() {
        this.switchToFrameParent();
        this.switchToFrame(this.frameToUseSelector);
        helper.waitForElementPresence(this.submitBtnSelector);
        return $(this.submitBtnSelector);
    }

    clickOnCloseBtn() {
        this.closeBtn.click();
        return new HomePage();
    }

    clickOnSubmitBtn() {
        this.submitBtn.waitForEnabled();
        this.submitBtn.click();
    }

}

module.exports = GetUpdatesModal;